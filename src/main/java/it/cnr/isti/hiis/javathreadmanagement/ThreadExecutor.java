package it.cnr.isti.hiis.javathreadmanagement;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Marco Manca
 */
public class ThreadExecutor {
        
    public static void main(String[] args) {
        System.out.println("[THREAD-EXECUTOR] start");
        ExecutorService executor = Executors.newFixedThreadPool(1);
        executor.submit(new MasterThread());
        executor.shutdown();
        System.out.println("[THREAD-EXECUTOR] end");        
    }
}

class MasterThread implements Runnable{
    private final int numThread = 3;
    private final int lowTime = 1;
    private final int highTime = 10;
    
    ExecutorService executor = Executors.newCachedThreadPool();
    ExecutorCompletionService<String> ecs = new ExecutorCompletionService<>(executor);
        
    @Override
    public void run() {
        System.out.println("[THREAD-MASTER] start");
        Random r = new Random();
        for(int i = 0; i < numThread; i++) {
            int waitTime = r.nextInt(highTime-lowTime) + lowTime;
            ecs.submit(new SlaveThread(String.valueOf(i), waitTime));
        }
        for(int i = 0; i < numThread; i++) {
            try {
                String result = ecs.take().get();
                System.out.println("[THREAD-MASTER] ".concat(result));
            } catch (InterruptedException ex) {
                Logger.getLogger(MasterThread.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExecutionException ex) {
                Logger.getLogger(MasterThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("[THREAD-MASTER] end");
    }
    
}

 class SlaveThread implements Callable<String> {

    String id;
    int waitTime;

    public SlaveThread(String id, int waitTime) {
        this.id = id;
        this.waitTime = waitTime;
    }
    
    

    
    @Override
    public String call() throws Exception {
        System.out.println("[THREAD-SLAVE] ID ".concat(id).concat(" start"));
        Thread.sleep(waitTime*1000);
        System.out.println("[THREAD-SLAVE] ID ".concat(id).concat(" end"));
        return "[THREAD-SLAVE] ID ".concat(id).concat(" finished after ").concat(String.valueOf(waitTime));
    }
    
}
